package com.example.locmapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Activity to map model action with robot specific actions.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class ActionMapping extends AppCompatActivity {
    private Domaindata data=new Domaindata();
    ArrayAdapter<String> adapter;
    ListView lstview;
    List<String> mappingArray= new ArrayList<>();
    MyAdapter lstadapter;
    Boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_mapping);

        Bundle b = this.getIntent().getBundleExtra("BUNDLE");
        if (b!=null)
            data = (Domaindata) b.getSerializable("data");


        lstview = (ListView) findViewById(R.id.lstView);
        Button ok1 = (Button) findViewById(R.id.okBtn);
        ok1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(ActionMapping.this,SaveActions.class);
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable) data);
                b.putSerializable("mapping", (Serializable)mappingArray);
                intent.putExtra("BUNDLE", b);
                startActivity(intent);
            }
        });
        Spinner mappingSpinner = (Spinner) findViewById(R.id.spinner3);
        ArrayList<String> rob_actions = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.yumi_actions)));
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, rob_actions);
        lstadapter = new MyAdapter(ActionMapping.this, mappingArray);
        lstview.setAdapter(lstadapter);

        mappingSpinner.setAdapter(adapter);
        mappingSpinner.setOnItemSelectedListener(new MappingSpinner());
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.lc_menu, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        menu.findItem(R.id.new_model).setEnabled(false);
        menu.findItem(R.id.save_model).setEnabled(true);
        menu.findItem(R.id.open).setEnabled(false);
        menu.findItem(R.id.run).setEnabled(false);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.new_model:
                startActivity(new Intent(this, Recording.class));
                return true;
            case R.id.open_model:
                return true;
            case R.id.save_model:
                //allactionSequences.put("temp", new ArrayList<>(current_seq));
                String ser = SerializeObject.objectToString(data);
                String filename = (data.domainName+".dat");
                if (ser != null && !ser.equalsIgnoreCase("")) {
                    SerializeObject.WriteSettings(ActionMapping.this, ser, filename);
                } else {
                    SerializeObject.WriteSettings(ActionMapping.this, "", filename);
                }
                SerializeObject.write(ActionMapping.this, data, data.domainName);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class MappingSpinner implements AdapterView.OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
            String robAction = parent.getItemAtPosition(pos).toString();
            if(!flag){
                flag=true;
                return;
            }
            if (pos!=0) {
                mappingArray.add(robAction);
                lstadapter.notifyDataSetChanged();
            }

        }
        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}