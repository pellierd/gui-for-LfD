package com.example.locmapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;

/**
 * This activity provides a layout to display the outputs of LOCM. It has three options to view PDDL
 * in text format, in graphical format (TO-DO) and to view the plot of finite state machines for each
 * type/sort.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */
public class ResultDisplay extends AppCompatActivity {
    private Domaindata data;
    private TextView textView1;
    private int flag;
    private Bitmap bmp;
    private HashMap<String, Bitmap> fsm = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_display);

        Bundle b = this.getIntent().getBundleExtra("BUNDLE");
        if (b!=null){
            data = (Domaindata) b.getSerializable("data");

            HashMap<String, byte[]> byter= data.output.getFiniteState();
            for (String key : byter.keySet()) {
                byte[] byteArray = byter.get(key);
                bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                fsm.put(String.valueOf(key), bmp);
            }
            //String name = data.domainName;
        }
        else
            data = (Domaindata) getIntent().getSerializableExtra("data");
        //System.out.println(data.output.sort.toString());
        ListView lst = (ListView) findViewById(R.id.lst);
        Button button = (Button) findViewById(R.id.pddlButton);
        textView1 = (TextView) findViewById(R.id.textView2);
        ImageView imgView = (ImageView) findViewById(R.id.imageView2);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                imgView.setVisibility(View.GONE);
                textView1.setVisibility(View.VISIBLE);
                String result = data.output.getDomain();
                HashMap<String, Collection<String>> srt = data.output.sort;
                int idx = result.indexOf(":types ") + 7;
                int last = result.indexOf(")", idx);
                String[] typ = result.substring(idx, last).split(" ");
                for (String pddlType:typ) {
                    for (String key:srt.keySet()){
                        if (srt.get(key).contains(pddlType)){
                            result = result.replaceAll(pddlType.toLowerCase(),key);}
                    }
                }
                data.output.setDomain(result);
                textView1.setText(result);
                flag=3;
            }
        });
        Button button2 = (Button) findViewById(R.id.fsmButton);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                textView1.setVisibility(View.GONE);
                imgView.setVisibility(View.VISIBLE);
                PopupMenu popup;
                popup = new PopupMenu(ResultDisplay.this, lst);
                for (String s : fsm.keySet()) {
                    popup.getMenu().add(s);
                }
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item1) {
                        bmp = fsm.get(item1.getTitle().toString());
                        imgView.setImageBitmap(bmp);
                        flag=2;
                        return true;
                    }
                });
                popup.show();}
        });

    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.lc_menu, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        menu.findItem(R.id.open_model).setEnabled(false);
        menu.findItem(R.id.new_model).setEnabled(false);
        menu.findItem(R.id.save_model).setEnabled(true);
        menu.findItem(R.id.run).setEnabled(true);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.new_model:
                startActivity(new Intent(this, Recording.class));
                return true;
            case R.id.run:
                Intent intent1 = new Intent(ResultDisplay.this,test.class);
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable)data);
                intent1.putExtra("BUNDLE", b);
                startActivity(intent1);
                return true;
            case R.id.save_model:
                String filename = "";
                File directory = new File(this.getExternalFilesDir(data.domainName), data.domainName);
                if (flag == 1) filename = "graphRepr.jpg";
                else if (flag == 2) filename = "fsm.txt";
                else if (flag == 3) filename = "pddl_domain.pddl";
                File file = new File(directory, filename);
                FileOutputStream fOut = null;
                try {
                    fOut = new FileOutputStream(file);
                    OutputStreamWriter osw = new OutputStreamWriter(fOut);
                    osw.write(textView1.getText().toString());
                    osw.flush();
                    osw.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            case R.id.open_model:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("file/*");
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }
}