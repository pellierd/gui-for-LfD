package com.example.locmapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * This main activity of the app. When the app is opened it starts from this window.
 * It has functionalities to create a project, load an existing project, load a PDDL model to modify
 * it by doing demonstrations (TO DO)
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */


public class MainActivity extends AppCompatActivity {
    public ArrayList<Action> domain_actions = new ArrayList<>();
    public Domaindata data = new Domaindata();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.lc_menu, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        menu.findItem(R.id.open_model).setEnabled(true);
        menu.findItem(R.id.save_model).setEnabled(false);
        menu.findItem(R.id.run).setEnabled(false);
        return true;
    }
    ActivityResultLauncher<Intent> launchSomeActivity = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data1 = result.getData();
                        Uri uri = data1.getData();
                        String filename = null;
                        int cut = uri.getPath().lastIndexOf('/');
                        if (cut != -1) {
                            filename = uri.getPath().substring(cut + 1);
                            System.out.println(filename);
                        }
                        Intent intent = new Intent(MainActivity.this, SaveActions.class);
                        Bundle b = new Bundle();
                        b.putSerializable("data", (Serializable) SerializeObject.readObject(MainActivity.this, filename));
                        intent.putExtra("BUNDLE", b);
                        startActivity(intent);
                    }
                }
            });
    ActivityResultLauncher<Intent> launchSomeActivity1 = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent dataFromRead = result.getData();
                        Uri uri = dataFromRead.getData();
                        String dom="";

                        try {
                            InputStream in = getContentResolver().openInputStream(uri);
                            BufferedReader r = new BufferedReader(new InputStreamReader(in));
                            StringBuilder total = new StringBuilder();
                            for (String line; (line = r.readLine()) != null; ) {
                                total.append(line).append('\n');
                            }
                            dom = total.toString();
                        }catch (Exception e) {

                        }

                        data.output.setDomain(dom);
                        Intent intent = new Intent(MainActivity.this, ResultDisplay.class);

                        Bundle b = new Bundle();
                        b.putSerializable("data", (Serializable)data);
                        intent.putExtra("BUNDLE", b);
                        startActivity(intent);
                    }
                }
            });

    @SuppressLint("NonConstantResourceId")
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.new_model:
                final EditText input = new EditText(MainActivity.this);
                new AlertDialog.Builder(MainActivity.this)
                        .setView(input)
                        .setTitle("Create New Folder")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {

                            }
                        })
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                String value = input.getText().toString();
                                File folder = new File(MainActivity.this.getExternalFilesDir(value),value);
                                //File folder = Environment.getExternalStorageDirectory(value);
                                if(!folder.exists())
                                {
                                    folder.mkdir();
                                    //System.out.println(folder.getAbsolutePath());
                                    data.domainName = value;
                                    data.actionSequences.put("temp", new ArrayList<>());
                                    String ser = SerializeObject.objectToString(data);
                                    String filename = (value+".dat");
                                    if (ser != null && !ser.equalsIgnoreCase("")) {
                                        SerializeObject.WriteSettings(MainActivity.this, ser, filename);
                                    } else {
                                        SerializeObject.WriteSettings(MainActivity.this, "", filename);
                                    }
                                    SerializeObject.write(MainActivity.this, data, value);

                                    Toast.makeText(MainActivity.this,"Folder Created!",Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(MainActivity.this, Recording.class);
                                    Bundle b = new Bundle();
                                    b.putSerializable("data", (Serializable) data);
                                    intent.putExtra("BUNDLE", b);
                                    startActivity(intent);
                                }
                                else {
                                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                                    alertDialog.setTitle("Project already exists");
                                    alertDialog.setMessage("A project with same name already exists: choose another name or open the project");
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                            (dialog1, which1) -> dialog1.dismiss());
                                    alertDialog.show();
                                }
                            }
                        }).show();
                return true;
            case R.id.open_model:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                Uri uri = Uri.parse("/SDCARD"); // a directory
                intent.setDataAndType(uri, "*/*");
                launchSomeActivity.launch(Intent.createChooser(intent, "Open folder"));
                return true;
            case R.id.open_pddl:
                Intent intent1 = new Intent(Intent.ACTION_GET_CONTENT);
                Uri uri1= Uri.parse("/SDCARD"); // a directory
                intent1.setDataAndType(uri1, "*/*");
                launchSomeActivity1.launch(Intent.createChooser(intent1, "Open folder"));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}