package com.example.locmapp;
import java.util.HashSet;

/**
 * This class implements an Predicates.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class Predicate {
    String name;
    HashSet<String> params=new HashSet<>();

    /**
     * Constructor specifying name of the predicate
     * @param name name of the predicate
     */
    public Predicate(String name){this.name=name;}

    /**
     * Constructor specifying name and parameters of the predicate
     * @param name name of the predicate
     * @param params parameters of the predicate
     */
    public Predicate(String name, HashSet<String> params){
        this.name=name;
        this.params = params;
    }

    /**
     * add a parameter
     * @param param the parameter to be be added
     */
    public void addParams(String param) {
        this.params.add(param);
    }

    /**
     * Gets the name of the predicate
     * @return the name of the predicate
     */
    public String getName(){return this.name;}

    /**
     * Renames the name of the predicate
     * @param name the new name of the predicate
     */
    public void rename(String name){this.name=name;}
}


