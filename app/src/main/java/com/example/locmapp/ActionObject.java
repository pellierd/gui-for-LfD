package com.example.locmapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements an Action.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class ActionObject implements Serializable {
    int noOfParameters;
    String actionName;
    List<String> mapping = new ArrayList<>();

    /**
     * Constructor specifying name of action and number of parameters that it takes
     * @param name name of action
     * @param parameters number of parameters of the action
     */
    public ActionObject(String name,int parameters)
    {
        this.noOfParameters=parameters;
        this.actionName=name;
    }

    /**
     *
     * @return the name of the action
     */
    public String getActionName(){return actionName;}

    /**
     * Sets the name of the action
     * @param name name of the action
     */
    public void setActionName(String name){this.actionName=name;}

    /**
     *
     * @return the number of parameters taken by the action
     */
    public int getNoOfParameters(){return noOfParameters;}

    /**
     * Sets the number of parameters of the action
     * @param params the number of parameters of the action
     */
    public void setNoOfParameters(int params) {this.noOfParameters=params;}

    /**
     * Sets the mapping of the action to a sequence of robot primitive actions
     * @param mapping sequence of robot primitive actions
     */
    public void setMapping(List<String> mapping) {this.mapping=mapping;}

    /**
     *
     * @return the mapping of action to robot primitive actions
     */
    public List<String> getMapping(){return this.mapping;}

}
