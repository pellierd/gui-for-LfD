package com.example.locmapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
/**
 * This activity displays a layout for the user to create action sequences. Provides an interface to
 * name/rename/remove an action and objects captured during demonstration.
 *
 * @author  Serawork Wallelign
 * @version 1.0
 * @since   2022-02-01
 */

public class SaveActions extends AppCompatActivity {
    private ArrayAdapter<String> actionAdapter;
    private ArrayAdapter<String> seqAdapter;
    public ArrayList<Action> current_seq =new ArrayList<>();
    private ArrayList<String> actions = new ArrayList<>();
    private Set<String> objList=new HashSet<>();
    private ListView listView;
    private List<ActionObject> domain_actions = new ArrayList<>();
    List< String > ListElementsArrayList=new ArrayList < String >();
    List<String> listSeqView =new ArrayList<>();
    private MyAdapter adapter;
    private Domaindata data=new Domaindata();
    private List<String> namesSeq;
    private MyAdapter adapter2;
    private Set<String> allObjList=new HashSet<>();
    private Map<String, ArrayList<Action>> allactionSequences= new HashMap<>();
    private ImageButton doneButton;
    private Animation mAnimation;
    String p1="";
    String p2="";
    List<String> mapping;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saveactions);

        Bundle b = this.getIntent().getBundleExtra("BUNDLE");
        if (b!=null){
            data = (Domaindata) b.getSerializable("data");
            p1 = (String) b.getSerializable("start");
            p2 = (String) b.getSerializable("end");
            mapping = (List<String>) b.getSerializable("mapping");
        }

        ListView l= findViewById(R.id.listView2);


        listView = findViewById(R.id.listView);
        allObjList = new HashSet<>(Arrays.asList(getResources().getStringArray(R.array.obj_array)));
        adapter = new MyAdapter(SaveActions.this, ListElementsArrayList);
        adapter2 = new MyAdapter(SaveActions.this, listSeqView);
        listView.setAdapter(adapter);
        l.setAdapter(adapter2);
        registerForContextMenu(listView);


        ImageButton startButton = findViewById(R.id.startButton2);

        doneButton = findViewById(R.id.doneButton2);
        Button saveButton = findViewById(R.id.save);
        Button modifyButton = findViewById(R.id.modify);
        Button deleteButton = findViewById(R.id.delete);
        doneButton.setEnabled(false);


        doneButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                data.actionSequences.put("temp", current_seq);
                Intent intent = new Intent(SaveActions.this, Locm.class);
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable)data);
                b.putSerializable("sequence", (Serializable) ListElementsArrayList);
                intent.putExtra("BUNDLE", b);
                startActivity(intent);
                v.clearAnimation();
            }
        });

        startButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                data.actionSequences.put("temp", current_seq);
                Intent intent = new Intent(SaveActions.this,Recording.class);
                Bundle b = new Bundle();
                b.putSerializable("data", (Serializable)data);
                intent.putExtra("BUNDLE", b);
                startActivity(intent);
            }
        });

        actions = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.actions_array)));
        namesSeq = new ArrayList<String>();
        namesSeq.add("Saved sequences");
        if (data.actionSequences!=null){
            allactionSequences = data.actionSequences;
            current_seq = allactionSequences.get("temp");
            List<String> out = seqDisp(current_seq);
            ListElementsArrayList.addAll(out);
            adapter.notifyDataSetChanged();
            namesSeq.addAll(data.getSeqNames());
            allObjList.addAll(data.getObjects());
            domain_actions = data.domain_actions;
        }
        else
            data.actionSequences=new HashMap<>();
        if (domain_actions != null) {
            for (int i = 0; i < domain_actions.size(); i++) {
                String act = domain_actions.get(i).getActionName();
                if (!actions.contains(act)) {
                    actions.add(act);
                }
            }
        }
        else
            domain_actions = new ArrayList<ActionObject>();


        Spinner actionSpinner = findViewById(R.id.actionSpinner);
        Spinner seqSpinner = findViewById(R.id.seqSpinner);

        actionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, actions);
        seqAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, namesSeq);
        actionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        seqAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        actionSpinner.setAdapter(actionAdapter);
        seqSpinner.setAdapter(seqAdapter);
        actionSpinner.setOnItemSelectedListener(new ActionSpinnerClass());

        seqSpinner.setOnItemSelectedListener(new SquenceSpinnerClass());

        mAnimation = new AlphaAnimation(1, 0);
        mAnimation.setDuration(200);
        mAnimation.setInterpolator(new LinearInterpolator());
        mAnimation.setRepeatCount(Animation.INFINITE);
        mAnimation.setRepeatMode(Animation.REVERSE);

        modifyButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                ListElementsArrayList.clear();
                String k = seqSpinner.getSelectedItem().toString();
                if (!k.equalsIgnoreCase("Saved sequences")) {
                    current_seq = new ArrayList<>(allactionSequences.get(k));
                    List<String> out = seqDisp(current_seq);
                    ListElementsArrayList.addAll(out);
                    adapter.notifyDataSetChanged();
                    for (int i = 0; i < current_seq.size(); i++) {
                        String act = current_seq.get(i).getActionName();
                        ArrayList<String> param = new ArrayList<>();
                        param = current_seq.get(i).getParameters();
                        String obj = "";
                        for (int j = 0; j < param.size(); j++) {
                            objList.add(param.get(j));
                            obj += param.get(j);
                            if (j == param.size() - 1)
                                obj += ")";
                            else
                                obj += ",";
                        }
                    }
                    if (seqEnough(allObjList, objList).equals(Arrays.asList("Enter objects"))) {
                        doneButton.setEnabled(true);
                        doneButton.startAnimation(mAnimation);
                    } else {
                        doneButton.setEnabled(false);
                        List<String> remaining = seqEnough(allObjList, objList);
                        remaining.remove("Enter objects");
                        Toast toast = Toast.makeText(SaveActions.this, "Give me actions with objects "+ remaining.toString(),Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 600);
                        toast.show();
                        doneButton.clearAnimation();
                    }

                }
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String k = seqSpinner.getSelectedItem().toString();
                if (!k.equalsIgnoreCase("Saved sequences")) {
                    allactionSequences.remove(k);
                    namesSeq.remove(k);
                    seqSpinner.setSelection(0);
                    listSeqView.clear();
                    adapter2.notifyDataSetChanged();
                }
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String newString = seqSpinner.getSelectedItem().toString();
                if (!newString.equalsIgnoreCase("Saved sequences"))
                    allactionSequences.put(newString, new ArrayList<>(current_seq));
                else {
                    final EditText input = new EditText(SaveActions.this);
                    final LinearLayout lila1 = new LinearLayout(SaveActions.this);
                    lila1.setOrientation(LinearLayout.VERTICAL);
                    final TextView tx = new TextView(SaveActions.this);
                    tx.append("Enter name of Sequence ,");
                    lila1.addView(input);
                    lila1.addView(tx);
                    new AlertDialog.Builder(SaveActions.this)
                            .setView(lila1)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Editable editable = input.getText();
                                    String newString = editable.toString();
                                    allactionSequences.put(newString, new ArrayList<>(current_seq));
                                    if (!namesSeq.contains(newString)) {
                                        namesSeq.add(newString);
                                        adapter2.notifyDataSetChanged();
                                    }
                                    data.actionSequences = allactionSequences;
                                    data.domain_actions = domain_actions;
                                    String ser = SerializeObject.objectToString(data);
                                    String filename = (data.domainName+".dat");
                                    if (ser != null && !ser.equalsIgnoreCase("")) {
                                        SerializeObject.WriteSettings(SaveActions.this, ser, filename);
                                    } else {
                                        SerializeObject.WriteSettings(SaveActions.this, "", filename);
                                    }
                                    SerializeObject.write(SaveActions.this, data, data.domainName);
                                    FileWriter writer = null;
                                    File directory = new File(SaveActions.this.getExternalFilesDir(data.domainName),"plan");
                                    if (!directory.exists()){
                                        directory.mkdirs();
                                    }
                                    filename = newString+".txt";
                                    File file = new File(directory, filename);
                                    try {
                                        writer = new FileWriter(file);
                                        for(String str: ListElementsArrayList) {
                                            writer.write(str + System.lineSeparator());
                                        }
                                        writer.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    // Do nothing.
                                }
                            }).show();

                }

            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.listView) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);
        }
    }

    /**
     * This class displays a popup window with options to edit the current action sequence.
     * It provides functionalities to Insert, Rename, Remove an action or append another sequence.
     * @param item selected option from the popup window
     * @return
     */

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (item.toString().equalsIgnoreCase("insert top")) {
            PopupMenu popup;
            popup = new PopupMenu(SaveActions.this,listView);
            for (String s : actions) {
                popup.getMenu().add(s);
            }
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item1) {
                    if (!item1.getTitle().toString().equalsIgnoreCase("Enter action name")) {
                        ListElementsArrayList.add(info.position, item1.getTitle().toString() + "(");
                        objPop(item1.getTitle().toString(), info.position);
                        if (seqEnough(allObjList, objList).equals(Arrays.asList("Enter objects"))) {
                            doneButton.setEnabled(true);
                            doneButton.startAnimation(mAnimation);
                        }
                        else {
                            doneButton.setEnabled(false);
                            List<String> remaining = seqEnough(allObjList, objList);
                            remaining.remove("Enter objects");
                            Toast toast = Toast.makeText(SaveActions.this, "Give me actions with objects "+ remaining.toString(),Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 600);
                            toast.show();
                            doneButton.clearAnimation();
                        }
                    }
                    return true;
                }
            });
            popup.show();
        }
        else if(item.toString().equalsIgnoreCase("insert bottom")) {
            PopupMenu popup;
            popup = new PopupMenu(SaveActions.this,listView);
            for (String s : actions) {
                popup.getMenu().add(s);
            }
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item1) {
                    if (!item1.getTitle().toString().equalsIgnoreCase("Enter action name")){
                        ListElementsArrayList.add(info.position+1, item1.getTitle().toString() + "(");
                        //adapter.notifyDataSetChanged();
                        objPop(item1.getTitle().toString(), info.position+1);
                        if (seqEnough(allObjList, objList).equals(Arrays.asList("Enter objects"))) {
                            doneButton.setEnabled(true);
                            doneButton.startAnimation(mAnimation);
                        }
                        else {
                            doneButton.setEnabled(false);
                            List<String> remaining = seqEnough(allObjList, objList);
                            remaining.remove("Enter objects");
                            Toast toast = Toast.makeText(SaveActions.this, "Give me actions with objects "+ remaining.toString(),Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 600);
                            toast.show();
                            doneButton.clearAnimation();
                        }
                    }
                    return true;
                }
            });
            popup.show();}
        else if(item.toString().equalsIgnoreCase("Rename")){
            PopupMenu popup;
            popup = new PopupMenu(SaveActions.this,listView);
            for (String s : actions) {
                popup.getMenu().add(s);
            }
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item1) {
                    if (!item1.getTitle().toString().equalsIgnoreCase("Enter action name")){
                        ListElementsArrayList.add(info.position, item1.getTitle().toString() + "(");
                        //adapter.notifyDataSetChanged();
                        objPop(item1.getTitle().toString(), info.position+1);
                        if (seqEnough(allObjList, objList).equals(Arrays.asList("Enter objects"))) {
                            doneButton.setEnabled(true);
                            doneButton.startAnimation(mAnimation);
                        }
                        else {
                            doneButton.setEnabled(false);
                            List<String> remaining = seqEnough(allObjList, objList);
                            remaining.remove("Enter objects");
                            Toast toast = Toast.makeText(SaveActions.this, "Give me actions with objects "+ remaining.toString(),Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 600);
                            toast.show();
                            doneButton.clearAnimation();
                        }
                    }
                    return true;
                }
            });
            popup.show();}

        else if(item.toString().equalsIgnoreCase("Add sequence")) {
            PopupMenu popup;
            popup = new PopupMenu(SaveActions.this,listView);
            for (String s : namesSeq) {
                popup.getMenu().add(s);
            }
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item1) {
                    if (!item1.getTitle().toString().equalsIgnoreCase("Saved sequences")){
                        String k = item1.getTitle().toString();
                        int i = info.position+1;
                        List<String> out= seqDisp(allactionSequences.get(k));
                        for (String disp:out) {
                            ListElementsArrayList.add(i, disp);
                            adapter.notifyDataSetChanged();
                            i++;
                        }
                        current_seq.addAll(info.position+1,allactionSequences.get(k));
                        objList.clear();
                        for (int ii = 0; ii < current_seq.size(); ii++) {
                            String act = current_seq.get(ii).getActionName();
                            ArrayList<String> param = new ArrayList<>();
                            param = current_seq.get(ii).getParameters();
                            String obj = "";
                            for (int j = 0; j < param.size(); j++) {
                                objList.add(param.get(j));
                                obj += param.get(j);
                            }
                        }
                        if (seqEnough(allObjList, objList).equals(Arrays.asList("Enter objects"))) {
                            doneButton.setEnabled(true);
                            doneButton.startAnimation(mAnimation);
                        }
                        else {
                            doneButton.setEnabled(false);
                            List<String> remaining = seqEnough(allObjList, objList);
                            remaining.remove("Enter objects");
                            Toast toast = Toast.makeText(SaveActions.this, "Give me actions with objects "+ remaining.toString(),Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 600);
                            toast.show();
                            doneButton.clearAnimation();
                        }
                    }
                    return true;
                }
            });
            popup.show();}
        else if (item.toString().equalsIgnoreCase("remove all")) {
            ListElementsArrayList.clear();
            current_seq.clear();
            objList.clear();
            adapter.notifyDataSetChanged();
            if (seqEnough(allObjList, objList).equals(Arrays.asList("Enter objects"))) {
                doneButton.setEnabled(true);
                doneButton.startAnimation(mAnimation);
            }
            else {
                doneButton.setEnabled(false);
                List<String> remaining = seqEnough(allObjList, objList);
                remaining.remove("Enter objects");
                Toast toast = Toast.makeText(SaveActions.this, "Give me actions with objects "+ remaining.toString(),Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 600);
                toast.show();
                doneButton.clearAnimation();
            }
        }

        else if (item.toString().equalsIgnoreCase("remove")) {
            ListElementsArrayList.remove(info.position);
            current_seq.remove(info.position);
            objList.clear();
            for (int ii = 0; ii < current_seq.size(); ii++) {
                String act = current_seq.get(ii).getActionName();
                ArrayList<String> param = new ArrayList<>();
                param = current_seq.get(ii).getParameters();
                String obj = "";
                for (int j = 0; j < param.size(); j++) {
                    objList.add(param.get(j));
                    obj += param.get(j);
                }
            }
            if (seqEnough(allObjList, objList).equals(Arrays.asList("Enter objects"))) {
                doneButton.setEnabled(true);
                doneButton.startAnimation(mAnimation);
            }
            else {
                doneButton.setEnabled(false);
                List<String> remaining = seqEnough(allObjList, objList);
                remaining.remove("Enter objects");
                Toast toast = Toast.makeText(SaveActions.this, "Give me actions with objects "+ remaining.toString(),Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 600);
                toast.show();
                doneButton.clearAnimation();
            }
            adapter.notifyDataSetChanged();}
        else if(item.toString().equalsIgnoreCase("recapture"))
            startActivity(new Intent(SaveActions.this, Recording.class));


        return false;
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.lc_menu, menu);
        return true;
    }
    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        menu.findItem(R.id.new_model).setEnabled(false);
        menu.findItem(R.id.save_model).setEnabled(true);
        menu.findItem(R.id.open).setEnabled(false);
        menu.findItem(R.id.run).setEnabled(false);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.new_model:
                startActivity(new Intent(this, Recording.class));
                return true;
            case R.id.open_model:
                return true;
            case R.id.save_model:
                //allactionSequences.put("temp", new ArrayList<>(current_seq));
                data.domain_actions=domain_actions;
                data.actionSequences=allactionSequences;
                String ser = SerializeObject.objectToString(data);
                String filename = (data.domainName+".dat");
                if (ser != null && !ser.equalsIgnoreCase("")) {
                    SerializeObject.WriteSettings(SaveActions.this, ser, filename);
                } else {
                    SerializeObject.WriteSettings(SaveActions.this, "", filename);
                }
                SerializeObject.write(SaveActions.this, data, data.domainName);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * A spinner class to choose among the existing sequences and display the action sequences in
     * a list view
     */
    class SquenceSpinnerClass implements OnItemSelectedListener {
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
            listSeqView.clear();

            if (!parent.getItemAtPosition(pos).toString().equalsIgnoreCase("Saved Sequences")) {
                ArrayList<Action> seqView= new ArrayList<>(allactionSequences.get(parent.getItemAtPosition(pos).toString()));
                if (seqView != null) for (int i = 0; i < seqView.size(); i++) {
                    String act = seqView.get(i).getActionName();
                    ArrayList<String> param = new ArrayList<>();
                    param = seqView.get(i).getParameters();
                    StringBuilder obj = new StringBuilder();
                    for (int j = 0; j < param.size(); j++) {
                        obj.append(param.get(j));
                        if (j == param.size() - 1)
                            obj.append(")");
                        else
                            obj.append(",");
                    }
                    listSeqView.add(act + "(" + obj.toString());
                }
            }adapter2.notifyDataSetChanged();


        }
        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    /**
     * A spinner class to choose already existing actions and select or add objects which are affected
     * by the action.
     * Displays the selected action in a list view
     */
    class ActionSpinnerClass implements OnItemSelectedListener {
        String numObj;
        String types;
        String preds;
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
            if (parent.getItemAtPosition(pos).toString().equalsIgnoreCase("Enter action name")) {
                // Set an EditText view to get user input
                final EditText input = new EditText(SaveActions.this);
                final LinearLayout lila1= new LinearLayout(SaveActions.this);
                lila1.setOrientation(LinearLayout.VERTICAL);
                final EditText input1 = new EditText(SaveActions.this);
                final EditText inputTypes = new EditText(SaveActions.this);
                final EditText inputPreds = new EditText(SaveActions.this);
                input1.setText(p1+","+p2);
                final TextView tx = new TextView(SaveActions.this);
                final TextView tx1 = new TextView(SaveActions.this);
                final TextView tx2 = new TextView(SaveActions.this);
                final TextView tx3 = new TextView(SaveActions.this);
                tx.append("Enter parameters separated by ,");
                tx1.append("Enter the name of the action");
                tx2.append("What are the types of the parameters");
                tx3.append("Hints for the state of these objects separated by ;");
                lila1.addView(tx1);
                lila1.addView(input);
                lila1.addView(tx);
                lila1.addView(input1);
                lila1.addView(tx2);
                lila1.addView(inputTypes);
                lila1.addView(tx3);
                lila1.addView(inputPreds);

                new AlertDialog.Builder(SaveActions.this)
                        .setView(lila1)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Editable editable = input.getText();
                                String newString = editable.toString();
                                Editable editable1 = input1.getText();
                                numObj = editable1.toString();
                                Editable editable2 = inputTypes.getText();
                                types = editable2.toString();
                                Editable editable3 = inputPreds.getText();
                                preds = editable3.toString();


                                if (containsItemName(actions, newString)) {
                                    AlertDialog alertDialog = new AlertDialog.Builder(SaveActions.this).create();
                                    alertDialog.setTitle("Action Exists");
                                    alertDialog.setMessage("Action name already exists. Please pick a different name.");
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //dialog.dismiss();

                                                }
                                            });
                                    alertDialog.show();

                                } else {
                                    String[] ao = numObj.split(",");
                                    String[] typ = types.split(",");
                                    String[] predicate = preds.split(";");
                                    data.output.predicates.put("s("+ newString, predicate);
                                    for (int ii=0; ii< typ.length; ii++){
                                        String tp = typ[ii].replace(" ","");
                                        if (data.output.sort != null && data.output.sort.containsKey(tp) && !data.output.sort.get(tp).contains(ao[ii].replace(" ","")))
                                            data.output.sort.get(tp).add(ao[ii].replace(" ",""));
                                        else if(!data.output.sort.containsKey(tp))
                                            data.output.sort.put(tp, new ArrayList<String>(Arrays.asList(ao[ii].replace(" ",""))));
                                    }
                                    actionAdapter.setNotifyOnChange(true);
                                    String chld = newString + "(" + numObj + ")";
                                    for (String s : ao) {
                                        objList.add(s);
                                    }
                                    allObjList.addAll(objList);
                                    ListElementsArrayList.add(chld);
                                    current_seq.add(new Action(newString, new ArrayList<String>(Arrays.asList(ao))));
                                    domain_actions.add(new ActionObject(newString, ao.length));
                                    actionAdapter.add(newString);
                                    adapter.notifyDataSetChanged();
                                    actionAdapter.notifyDataSetChanged();
                                    if (seqEnough(allObjList, objList).equals(Arrays.asList("Enter objects"))) {
                                        doneButton.setEnabled(true);
                                        doneButton.startAnimation(mAnimation);
                                    }
                                    else {
                                        doneButton.setEnabled(false);
                                        List<String> remaining = seqEnough(allObjList, objList);
                                        remaining.remove("Enter objects");
                                        Toast toast = Toast.makeText(SaveActions.this, "Give me actions with objects "+ remaining.toString(),Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 600);
                                        toast.show();
                                        doneButton.clearAnimation();
                                    }
                                    data.domain_actions=domain_actions;
                                    data.actionSequences.put("temp", current_seq);
                                    Intent intent = new Intent(SaveActions.this, ActionMapping.class);
                                    Bundle b = new Bundle();
                                    b.putSerializable("data", (Serializable) data);
                                    intent.putExtra("BUNDLE", b);
                                    startActivity(intent);
                                }
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // Do nothing.
                            }
                        }).show();
            } else {
                String newString = parent.getItemAtPosition(pos).toString();
                //String chld = getAction(newString);
                //String[] ao = chld.split("(,)");
                ListElementsArrayList.add(newString + "(");
                adapter.notifyDataSetChanged();
                objPop(newString, ListElementsArrayList.size()-1);
            }

        }
        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    /**
     * This class lists all the existing objects in a popup window and displays the selected objects
     * together with the action in a list view
     * @param newString a string containing name of the actions and objects separated by comma
     * @param pos position of the action in the list
     */
    public void objPop(String newString, int pos) {
        int numObj = getParam(newString);
        ArrayList<String> ao = new ArrayList<>();
        for (int i = 0; i < numObj; i++) {
            PopupMenu popup;
            popup = new PopupMenu(SaveActions.this, listView);
            for (String s : allObjList)
                popup.getMenu().add(s);
            

            int finalI = i;

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item1) {
                    if (!item1.getTitle().toString().equalsIgnoreCase("Enter objects")) {
                        ao.add(item1.getTitle().toString());
                        String str = ListElementsArrayList.get(pos);
                        str += item1.getTitle().toString();
                        objList.add(item1.getTitle().toString());
                        if (finalI == 0) {
                            str += ")";
                            if (seqEnough(allObjList, objList).equals(Arrays.asList("Enter objects"))) {
                                doneButton.setEnabled(true);
                                doneButton.startAnimation(mAnimation);
                            }
                            else {
                                doneButton.setEnabled(false);
                                List<String> remaining = seqEnough(allObjList, objList);
                                remaining.remove("Enter objects");
                                Toast toast = Toast.makeText(SaveActions.this, "Give me actions with objects "+ remaining.toString(),Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 600);
                                toast.show();
                                doneButton.clearAnimation();
                            }
                        }
                        else
                            str += ",";


                        ListElementsArrayList.set(pos, str);
                        adapter.notifyDataSetChanged();
                        //ListElementsArrayList.add(str);
                    } else {
                        final EditText input = new EditText(SaveActions.this);
                        final LinearLayout lila1 = new LinearLayout(SaveActions.this);
                        lila1.setOrientation(LinearLayout.VERTICAL);
                        final EditText input1 = new EditText(SaveActions.this);
                        final TextView tx = new TextView(SaveActions.this);
                        final TextView tx1 = new TextView(SaveActions.this);
                        tx.append("Enter object");
                        tx1.append("Type of Object");
                        lila1.addView(tx);
                        lila1.addView(input1);
                        lila1.addView(tx1);
                        lila1.addView(input);

                        new AlertDialog.Builder(SaveActions.this)
                                .setView(lila1)
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        Editable editable1 = input1.getText();
                                        Editable editable2 = input.getText();
                                        if (!input1.getText().toString().equals("")) {
                                            String str = ListElementsArrayList.get(pos);
                                            str += editable1.toString();
                                            objList.add(editable1.toString());
                                            allObjList.add(editable1.toString());
                                            if (finalI == 0) {
                                                str += ")";
                                                if (seqEnough(allObjList, objList).equals(Arrays.asList("Enter objects"))) {
                                                    doneButton.setEnabled(true);
                                                    doneButton.startAnimation(mAnimation);
                                                } else {
                                                    doneButton.setEnabled(false);
                                                    List<String> remaining = seqEnough(allObjList, objList);
                                                    remaining.remove("Enter objects");
                                                    Toast toast = Toast.makeText(SaveActions.this, "Give me actions with objects " + remaining.toString(), Toast.LENGTH_LONG);
                                                    toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 600);
                                                    toast.show();
                                                    doneButton.clearAnimation();
                                                }
                                            }
                                            else
                                                str += ",";
                                            ListElementsArrayList.set(pos, str);


                                            //allObjList.add(editable1.toString());
                                            ao.add(editable1.toString());
                                            adapter.notifyDataSetChanged();

                                            if(data.output.sort!=null && data.output.sort.containsKey(editable2.toString()))
                                                data.output.sort.get(editable2.toString()).add(editable1.toString());
                                            else if (!data.output.sort.containsKey(editable2.toString())){
                                                data.output.sort.put(editable2.toString(), new ArrayList<String>(Arrays.asList(editable1.toString())));
                                            }
                                        }
                                    }
                                })
                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                }).show();
                    }
                    return false;
                }
            });
            popup.show();
        }
        current_seq.add(new Action(newString, ao));

    }

    /**
     * Class to return the number of objects given the name of the action
     * @param name action name
     * @return the number of objects for an action
     */
    public int getParam(String name){
        int numObj=0;
        for (int i = 0; i < domain_actions.size(); ++i)
        {
            if (domain_actions.get(i).getActionName().equalsIgnoreCase(name)){
                numObj = domain_actions.get(i).getNoOfParameters();
                break;
            }
        }
        return numObj;
    }


    /**
     * Checks if a String exists in a list of Strings
     * @param items list of Strings
     * @param name a String
     * @return true if the list contains the given string, false otherwise
     */
    public boolean containsItemName(ArrayList<String> items, String name) {
        for (String item : items) {
            if (item.equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    /**
     * This class takes a list of Actions and converts it to list of Strings formatted to be displayed
     * in a list view
     * @param curr_seq list of Actions
     * @return list of Strings edited in a more readable format
     */
    public List<String> seqDisp(ArrayList<Action> curr_seq){
        List<String> out = new ArrayList<>();
        for (int i = 0; i < curr_seq.size(); i++) {
            String act = curr_seq.get(i).getActionName();
            ArrayList<String> param = new ArrayList<>();
            param = curr_seq.get(i).getParameters();
            StringBuilder obj = new StringBuilder();
            for (int j = 0; j < param.size(); j++) {
                obj.append(param.get(j));
                if (j == param.size() - 1)
                    obj.append(")");
                else
                    obj.append(",");
            }
            out.add(act + "(" + obj.toString());
        }
        return out;
    }

    /**
     * Checks the action sequence entered by the user is sufficient to train LOCM
     * @param objSet the total set of objects present in all the sequences
     * @param currentObjset the set of objects in the current action sequence
     * @return a list of objects that do not exist in the current sequence but in the total set of objects
     */
    public static List<String> seqEnough(Set<String> objSet, Set<String> currentObjset){
        Set<String> big = new HashSet<>(objSet);
        Set<String> cur = new HashSet<>(currentObjset);
        big.removeAll(cur);
        List<String> missingObj = new ArrayList<>();
        for (String obj:big){
            missingObj.add(obj);
        }
        return missingObj;
    }
}

