import sys
import RWSwrapper as RWS

rws = RWS.RWSwrapper()


def deactivate(arm="ROB_L"): #option=0
    if(rws.connect()):
        # deactivate LeadThrough Mode for Right Arm
        rws.deactivateLeadThrough("ROB_L") # ROB_L

def getrobtarget(arm="ROB_L"): #option=1
    finalPos = rws.getRobtarget(arm)
    print(finalPos)

def activate(arm="ROB_L"): #option=2
    if(rws.connect()):
        # activate LeadThrough Mode for Right Arm
        rws.activateLeadThrough("ROB_L") # ROB_L

def getStatus(arm="ROB_L"): #option=3
    if(rws.connect()):
        status = rws.getLeadThroughStatus("ROB_L")
        print(status)

if __name__ == '__main__':
    if len(sys.argv)<2:
        print("usage: python3 rws.py arm option")
    else:
        if len(sys.argv)==2:
            option=sys.argv[-1]
            arm = "ROB_L"
        else:
            option=sys.argv[-1]
            arm=sys.argv[1]
        if option=="0":
            deactivate(arm)
        if option=="1":
            getrobtarget(arm)
        if option=="2":
            activate(arm)
        if option=="3":
            getStatus(arm)

    #pointonair = [0.0138, -0.4415, -0.0668, 0.9147, 1.3711, 0.8531, -2.9349] [0.0183, -0.4463, -0.0694, 0.9154, 1.3696, 0.8538, -2.9328]
    #p1 = [0.0969, 0.4228, 0.4226, 0.2795, 1.6001, 0.3511, -3.0736]
    #p2 = [0.0182, 0.5333, 0.3705, 0.2167, 1.6989, 0.4238, -3.2906]
    #p3=[0.0181, 0.4729, 0.5486, 0.1191, 1.7475, 0.2586, -3.3107]
    #p4 U=[0.0144, 0.4773, 0.3677, 0.0543, 1.2517, 0.4094, -2.7717] L=[0.0144, 0.5675, 0.4254, -0.0188, 1.2882, 0.347, -2.8856]



