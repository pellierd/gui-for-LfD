#! /bin/bash
if [[ "$#" -lt 1 || "$#" -gt 3 ]]; then
    echo "Illegal number of parameters"
fi
cd /home/sera/Downloads/ABB_RWS_REST_API
python3 rws.py $1 $2
